ALTER TABLE book ADD COLUMN book_tsvector tsvector
    GENERATED ALWAYS AS (to_tsvector('english', book_name)) STORED;

CREATE INDEX ts_book_name_idx ON book_name USING GIN (book_tsvector);

SELECT book_name FROM book WHERE book.book_name @@ to_tsquery('1');

SELECT * FROM book WHERE to_tsvector(book_name) @@ to_tsquery(:text);
