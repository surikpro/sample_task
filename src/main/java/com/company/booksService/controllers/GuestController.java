package com.company.booksService.controllers;

import com.company.booksService.domain.dto.guest.GuestCreateDto;
import com.company.booksService.domain.dto.guest.GuestDto;
import com.company.booksService.services.GuestsService;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a REST API class for Books Tracking system.
 */
@RestController
@RequestMapping()
@RequiredArgsConstructor
public class GuestController {

    private final GuestsService guestsService;

    /**
     * It creates a guest
     *
     * @return a created GuestDto on JSON format
     */
    @PostMapping(path = "/guests")
    public GuestDto create(@RequestBody GuestCreateDto guestCreateDto) {
        return guestsService.create(guestCreateDto);
    }

    /**
     * It deletes a Guest
     *
     * @return void
     */
    @DeleteMapping("/guests/{guestId}")
    public void delete(@PathVariable Long guestId) {
        guestsService.delete(guestId);
    }
}
