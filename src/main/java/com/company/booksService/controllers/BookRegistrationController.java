package com.company.booksService.controllers;

import com.company.booksService.services.BookRegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class BookRegistrationController {

    private final BookRegistrationService bookRegistrationService;

    /**
     * Guest takes a Book
     *
     * @return void
     */
    @PostMapping("/guests/{guestId}/books/{bookId}/register")
    public void takeBook(@PathVariable(name = "guestId") Long guestId,
                         @PathVariable(name = "bookId") Long bookId) {
        bookRegistrationService.register(guestId, bookId);
    }

    /**
     * Guest gives back a Book
     *
     * @return void
     */
    @PostMapping("/guests/{guestId}/books/{bookId}/unregister")
    public void giveBackBook(@PathVariable(name = "guestId") Long guestId,
                             @PathVariable(name = "bookId") Long bookId) {
        bookRegistrationService.unregister(guestId, bookId);
    }
}
