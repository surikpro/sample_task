package com.company.booksService.controllers;

import com.company.booksService.services.CoversService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * by Aydar Zakirov
 * since 14.02.2022
 * This controller deals with book cover images and handles its upload
 * It provides such services as getting file by Id and uploading files from outer source
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/files")
public class CoversController {

    private final CoversService coversService;

    @GetMapping("/{cover-id}")
    public ResponseEntity getFile(@PathVariable("cover-id") Long coverId, HttpServletResponse response) {
        return coversService.addFileToResponse(coverId, response);
    }

    @PostMapping("/upload")
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile cover,
                                     @RequestParam("bookId") Long bookId) {
        coversService.saveFile(cover, bookId);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
