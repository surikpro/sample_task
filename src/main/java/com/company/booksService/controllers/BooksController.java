package com.company.booksService.controllers;

import com.company.booksService.domain.dto.book.BookCreateDto;
import com.company.booksService.domain.dto.book.BookDto;
import com.company.booksService.domain.dto.book.BookUpdateDto;
import com.company.booksService.domain.entity.Book;
import com.company.booksService.domain.mapper.BookMapper;
import com.company.booksService.services.BooksService;
import com.company.booksService.services.CoversService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.springframework.http.HttpStatus.NO_CONTENT;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a REST API class for Books Tracking system.
 */
@RestController
@RequiredArgsConstructor
@Tag(name = "Book", description = "description")
@ApiResponse(responseCode = "500", description = "Internal error")
@ApiResponse(responseCode = "404", description = "Book not found")
public class BooksController {

    private final BooksService booksService;
    private final CoversService coversService;
    private final BookMapper bookMapper;

    /**
     * It finds and return a BookDto on JSON format if Book exists
     *
     * @return a bookDto on JSON format
     */
    @Operation(description = "Find book by id")
    @ApiResponse(responseCode = "200", description = "Book found")
    @ApiResponse(responseCode = "404", description = "Book not found")
    @GetMapping("/books/{bookId}")
    public BookDto getById(@PathVariable(name = "bookId") Long bookId) {
        return bookMapper.toBookDto(booksService.get(bookId));
    }

    /**
     * It creates a new Book
     *
     * @return a bookDto on JSON format
     */
    @Operation(description = "Create new book")
    @ApiResponse(responseCode = "200", description = "Book created")
    @PostMapping("/books")
    public BookDto create(@RequestBody BookCreateDto bookCreateDto) {
        return booksService.create(bookCreateDto);
    }

    /**
     * It updates a book if it exists
     *
     * @return a bookDto on JSON format
     */
    @Operation(description = "Update book by id")
    @ApiResponse(responseCode = "200", description = "Book updated")
    @ApiResponse(responseCode = "404", description = "Book not found")
    @PatchMapping("/books/{bookId}")
    public Book update(@PathVariable(name = "bookId") Long bookId,
                       @RequestBody BookUpdateDto bookUpdateDto) {
        return booksService.update(bookId, bookUpdateDto);
    }

    /**
     * It removes a Book if it exists
     *
     * @return void
     */
    @Operation(description = "Remove book by id")
    @ApiResponse(responseCode = "204", description = "Book removed")
    @DeleteMapping("/books/{bookId}")
    @ResponseStatus(value = NO_CONTENT)
    public void delete(@PathVariable(name = "bookId") Long bookId) {
        booksService.delete(bookId);
    }

    /**
     * Return list of Books on JSON format
     *
     * @return books on JSON format
     */
    @GetMapping(path = "/guests/{guestId}/booksTaken")
    public List<Book> get(@PathVariable Long guestId) {
        return booksService.getAllBooksOfGuest(guestId);
    }

    @GetMapping(path = "/books/{bookId}/cover")
    public ResponseEntity getCoverByBook(@PathVariable("bookId") Long bookId, HttpServletResponse response) {
        return coversService.getCoverByBook(bookId, response);
    }
}
