package com.company.booksService.controllers;

import com.company.booksService.domain.dto.bookregistration.BookRegistrationDto;
import com.company.booksService.domain.entity.Book;
import com.company.booksService.services.SearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class SearchController {

    private final SearchService searchService;

    @GetMapping("/search/simple-dimple/{byName}")
    public List<Book> searchBookName(@PathVariable("byName") String text) {
        return searchService.findBooksByBookName(text);
    }

    @GetMapping("/search/full/{byFullText}")
    public List<Book> searchByFullText(@PathVariable("byFullText") String text) {
        return searchService.findBooksByFullText(text);
    }

    @GetMapping("/search/{from-date}/{to-date}")
    public List<BookRegistrationDto> searchByDatesBetween(@PathVariable("from-date") String fromDate,
                                                          @PathVariable("to-date") String toDate) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime from = LocalDateTime.parse(fromDate, dateTimeFormatter);
        LocalDateTime to = LocalDateTime.parse(toDate, dateTimeFormatter);
        return searchService.findBookRegistrationsByDates(from, to);
    }

    @GetMapping("/search/books/byGuestId")
    public List<BookRegistrationDto> searchRegistrationsByGuestId(@RequestParam("guestId") Long guestId) {
        return searchService.findRegistrationsByGuestId(guestId);
    }
}
