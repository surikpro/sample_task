package com.company.booksService.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is my Custom GuestNotFound class for Book Tracking system.
 */
@ResponseStatus(value = NOT_FOUND)
public class GuestNotFoundException extends RuntimeException {
    public GuestNotFoundException(Long guestId) {
        super("Guest with id: " + guestId + " is not found");
    }
}
