package com.company.booksService.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is my Custom BookNotFound class for Book Tracking system.
 */
@ResponseStatus(value = NOT_FOUND)
public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(Long bookId) {
        super("Book with id: " + bookId + " is not found");
    }
    public BookNotFoundException() {
        super("Books are not found");
    }
}

