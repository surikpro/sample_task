package com.company.booksService.services;

import com.company.booksService.domain.dto.guest.GuestCreateDto;
import com.company.booksService.domain.dto.guest.GuestDto;
import com.company.booksService.domain.entity.Guest;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Guests Service Interface.
 */
public interface GuestsService {

    GuestDto create(GuestCreateDto guestCreateDto);

    void delete(Long guestId);

    Guest getById(Long guestId);

}
