package com.company.booksService.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;

/**
  * Covers Service Interface
 */
public interface CoversService {
    void saveFile(MultipartFile file, Long bookId);

    ResponseEntity<?> addFileToResponse(Long coverId, HttpServletResponse response);

    ResponseEntity getCoverByBook(Long bookId, HttpServletResponse response);

}
