package com.company.booksService.services;

import com.company.booksService.domain.entity.CoverInfo;
import com.company.booksService.exceptions.BookNotFoundException;
import com.company.booksService.repositories.BooksRepository;
import com.company.booksService.repositories.CoversRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

/**
 * Implementation of Service Interface
 */
@RequiredArgsConstructor
@Service
@Transactional
public class CoversServiceImpl implements CoversService {

    private final CoversRepository coversRepository;
    private final BooksRepository booksRepository;

    @Value("${storage.folder}")
    private String storageFolder;

    @Override
    public void saveFile(MultipartFile file, Long bookId) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        CoverInfo coverInfo = CoverInfo.builder()
                .mimeType(file.getContentType())
                .originalFileName(file.getOriginalFilename())
                .storageFileName(UUID.randomUUID() + "." + extension)
                .size(file.getSize())
                .build();
        coversRepository.save(coverInfo);
        booksRepository.getById(bookId).setCoverInfo(coverInfo);

        try {
            Files.copy(file.getInputStream(), Paths.get(storageFolder, coverInfo.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public ResponseEntity addFileToResponse(Long coverId, HttpServletResponse response) {
        Optional<CoverInfo> coverInfo = coversRepository.findById(coverId);
        response.setContentType(coverInfo.get().getMimeType());
        response.setContentLength(coverInfo.get().getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + coverInfo.get().getOriginalFileName() + "\"");
        try {
            IOUtils.copy(new FileInputStream(storageFolder + "/" + coverInfo.get().getStorageFileName()), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }

    @Override
    public ResponseEntity getCoverByBook(Long bookId, HttpServletResponse response) {
        Optional<CoverInfo> coverInfo = Optional.of(coversRepository.findCoverInfoByBook_BookId(bookId)).orElseThrow(() -> new BookNotFoundException());
        response.setContentType(coverInfo.get().getMimeType());
        response.setContentLength(coverInfo.get().getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + coverInfo.get().getOriginalFileName() + "\"");
        try {
            IOUtils.copy(new FileInputStream(storageFolder + "/" + coverInfo.get().getStorageFileName()), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }
}
