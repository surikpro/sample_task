package com.company.booksService.services;

import com.company.booksService.domain.entity.Book;
import com.company.booksService.domain.entity.BookRegistration;
import com.company.booksService.domain.entity.Guest;
import com.company.booksService.repositories.BookRegistrationRepository;
import com.company.booksService.util.QuantityHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a BooksRegistration Service Class for business logic.
 * Operations are provided: Guest takes a book and gives it back
 */

@Service
@RequiredArgsConstructor
public class BookRegistrationServiceImpl implements BookRegistrationService {
    private final GuestsService guestsService;
    private final BooksService booksService;
    private final QuantityHandler quantityHandler;
    private final BookRegistrationRepository bookRegistrationRepository;

    @Override
    public void register(Long guestId, Long bookId) {
        Guest guest = guestsService.getById(guestId);
        Book book = booksService.get(bookId);
        if (quantityHandler.checkBookQuantity(bookId)) {
            quantityHandler.decrementBookQuantity(bookId);
        } else {
            System.out.println("All books are taken");
        }
        BookRegistration bookRegistration = new BookRegistration();
        bookRegistration.setGuest(guest);
        bookRegistration.setBook(book);
        bookRegistration.setState(BookRegistration.State.TAKEN);
        bookRegistration.setRegisteredAt(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        bookRegistrationRepository.save(bookRegistration);
    }

    @Override
    public void unregister(Long guestId, Long bookId) {
        List<BookRegistration> bookRegistrations = bookRegistrationRepository.findBookRegistrationsByGuest_GuestId(guestId);
        for (BookRegistration bookRegistration : bookRegistrations) {
            if (bookRegistration.getBook().getBookId().equals(bookId)) {
                bookRegistration.setState(BookRegistration.State.RETURNED);
                bookRegistration.setUnregisteredAt(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
                bookRegistrationRepository.save(bookRegistration);
                quantityHandler.incrementBookQuantity(bookId);
            }
        }
    }
}
