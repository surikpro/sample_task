package com.company.booksService.services;

import com.company.booksService.domain.dto.bookregistration.BookRegistrationDto;
import com.company.booksService.domain.entity.Book;
import com.company.booksService.repositories.BookRegistrationRepository;
import com.company.booksService.repositories.SearchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.company.booksService.domain.dto.bookregistration.BookRegistrationDto.toBookRegistrationDtoList;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Search Service Implementation Class for business logic.
 * Operations are provided: Find a book or a list of books
 * by providing a text as a part of its Name
 */
@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final SearchRepository searchRepository;
    private final BookRegistrationRepository bookRegistrationRepository;

    @Override
    public List<Book> findBooksByBookName(String text) {
        return searchRepository.findByBookNameLike("%" + text + "%");
    }

    @Override
    public List<Book> findBooksByFullText(String text) {
        return searchRepository.findBooks(text);
    }

    @Override
    public List<BookRegistrationDto> findBookRegistrationsByDates(LocalDateTime from, LocalDateTime to) {

        return toBookRegistrationDtoList(bookRegistrationRepository.findAllByRegisteredAtBetween(from, to));
    }

    @Override
    public List<BookRegistrationDto> findRegistrationsByGuestId(Long guestId) {
        return toBookRegistrationDtoList(bookRegistrationRepository.findBookRegistrationsByGuest_GuestId(guestId));
    }
}
