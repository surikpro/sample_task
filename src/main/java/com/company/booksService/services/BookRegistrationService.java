package com.company.booksService.services;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a BooksRegistration Service Interface.
 */

public interface BookRegistrationService {
    void register(Long guestId, Long bookId);
    void unregister(Long guestId, Long bookId);
}
