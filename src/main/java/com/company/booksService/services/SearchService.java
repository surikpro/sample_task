package com.company.booksService.services;

import com.company.booksService.domain.dto.bookregistration.BookRegistrationDto;
import com.company.booksService.domain.entity.Book;
import com.company.booksService.domain.entity.BookRegistration;

import java.time.LocalDateTime;
import java.util.List;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Search Service Interface for business logic.
 * Operations are provided: Find a book or a list of books
 * by providing a text as a part of its Name
 */
public interface SearchService {
    List<Book> findBooksByBookName(String text);

    List<Book> findBooksByFullText(String text);

    List<BookRegistrationDto> findBookRegistrationsByDates(LocalDateTime from, LocalDateTime to);

    List<BookRegistrationDto> findRegistrationsByGuestId(Long guestId);
}
