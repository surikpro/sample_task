package com.company.booksService.services;

import com.company.booksService.domain.dto.book.BookCreateDto;
import com.company.booksService.domain.dto.book.BookDto;
import com.company.booksService.domain.dto.book.BookUpdateDto;
import com.company.booksService.domain.entity.Book;

import java.util.List;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Book Service Interface.
 */
public interface BooksService {
    Book get(Long bookId);

    BookDto create(BookCreateDto bookCreateDto);

    Book update(Long bookId, BookUpdateDto bookUpdateDto);

    void delete(Long bookId);

    List<Book> getAllBooksOfGuest(Long guestId);

    void save(Book book);
}
