package com.company.booksService.services;

import com.company.booksService.domain.dto.guest.GuestCreateDto;
import com.company.booksService.domain.dto.guest.GuestDto;
import com.company.booksService.domain.entity.Guest;
import com.company.booksService.domain.mapper.GuestMapper;
import com.company.booksService.exceptions.GuestNotFoundException;
import com.company.booksService.repositories.GuestsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;


/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Guests Service Class for business logic.
 * Operations are provided: getting, creating a guest, saving, updating and deleting it
 */
@Service
@RequiredArgsConstructor
public class GuestsServiceImpl implements GuestsService {

    private final GuestsRepository guestsRepository;
    private final GuestMapper guestMapper;

    @Override
    public GuestDto create(GuestCreateDto guestCreateDto) {
        return Optional.ofNullable(guestCreateDto)
                .map(guestMapper::fromCreateDto)
                .map(guestsRepository::save)
                .map(guestMapper::toGuestDto)
                .orElseThrow();
    }

    @Override
    public void delete(Long guestId) {
        guestsRepository.deleteById(guestId);
    }

    @Override
    public Guest getById(Long guestId) {
        return Optional.of(guestsRepository.getById(guestId)).
                orElseThrow(() -> new GuestNotFoundException(guestId));
    }
}
