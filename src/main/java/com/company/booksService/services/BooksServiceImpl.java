package com.company.booksService.services;

import com.company.booksService.domain.dto.book.BookCreateDto;
import com.company.booksService.domain.dto.book.BookDto;
import com.company.booksService.domain.dto.book.BookUpdateDto;
import com.company.booksService.domain.entity.Book;
import com.company.booksService.domain.entity.BookRegistration;
import com.company.booksService.domain.entity.Guest;
import com.company.booksService.domain.mapper.BookMapper;
import com.company.booksService.exceptions.BookNotFoundException;
import com.company.booksService.repositories.BookRegistrationRepository;
import com.company.booksService.repositories.BooksRepository;
import com.company.booksService.repositories.GuestsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Books Service Class for business logic.
 * Operations are provided: getting a book by Id, creating a book,
 * updating and deleting it, saving and getting all books of a Guest
 */
@Service
@RequiredArgsConstructor
public class BooksServiceImpl implements BooksService {

    private final BooksRepository booksRepository;
    private final BookRegistrationRepository bookRegistrationRepository;
    private final BookMapper bookMapper;

    @Override
    public Book get(Long bookId) {
        return Optional.ofNullable(bookId)
                .map(booksRepository::getById)
                .orElseThrow(() -> new BookNotFoundException(bookId));
    }

    @Override
    public BookDto create(BookCreateDto bookCreateDto) {
        return Optional.ofNullable(bookCreateDto)
                .map(bookMapper::fromCreateDto)
                .map(booksRepository::save)
                .map(bookMapper::toBookDto)
                .orElseThrow();
    }

    @Override
    public Book update(Long bookId, BookUpdateDto bookUpdateDto) {
        return Optional.ofNullable(bookUpdateDto)
                .map(bookMapper::fromUpdateDto)
                .map(it -> bookMapper.merge(booksRepository.getById(bookId), it))
                .map(booksRepository::save)
                .orElseThrow();
    }

    @Override
    public void delete(Long bookId) {
        final Book toDelete = booksRepository.findById(bookId).
                orElseThrow(() -> new BookNotFoundException(bookId));
        booksRepository.delete(toDelete);
    }

    @Override
    public List<Book> getAllBooksOfGuest(Long guestId) {
        List<Book> books = new ArrayList<>();
        List<BookRegistration> registrations = bookRegistrationRepository.getAllBooksByGuest_GuestId(guestId);
        for (BookRegistration bookRegistration : registrations) {
            if (bookRegistration.getState().equals(BookRegistration.State.TAKEN)) {
                books.add(bookRegistration.getBook());
            }
        }
        return books;
//        if (guestsRepository.findById(guestId).isPresent()) {
//            Guest guest = guestsService.getById(guestId);
//            return bookMapper.toListBookDto(null);
//        } else {
//            throw new GuestNotFoundException(guestId);
//        }
    }

    @Override
    public void save(Book book) {
        booksRepository.save(book);
    }
}
