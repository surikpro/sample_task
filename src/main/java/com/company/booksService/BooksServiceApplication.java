package com.company.booksService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Main method for starting the application.
 */
@SpringBootApplication
public class BooksServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BooksServiceApplication.class, args);

    }
}
