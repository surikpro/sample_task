package com.company.booksService.domain.dto.guest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import static lombok.AccessLevel.PRIVATE;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a GuestCreateDto class for Book Tracking system.
 */
@Value
@Builder
@Jacksonized
@AllArgsConstructor(access = PRIVATE)
public class GuestCreateDto {

    String guestName;
}
