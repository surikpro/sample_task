package com.company.booksService.domain.dto.guest;

import lombok.*;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a GuestDto class for Book Tracking system.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GuestDto {

    private Long guestId;
    private String guestName;
}