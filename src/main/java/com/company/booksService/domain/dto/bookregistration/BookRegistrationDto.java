package com.company.booksService.domain.dto.bookregistration;

import com.company.booksService.domain.entity.Book;
import com.company.booksService.domain.entity.BookRegistration;
import com.company.booksService.domain.entity.Guest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a BookRegistrationDto class for Books Tracking system.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookRegistrationDto {
    public enum State {
        TAKEN, RETURNED
    }
    private Long bookRegistrationId;

    private Guest guest;

    private Book book;

    private LocalDateTime registeredAt;

    private LocalDateTime unregisteredAt;

    @Enumerated(value = EnumType.STRING)
    private BookRegistration.State state;

    public static BookRegistrationDto toBookRegistrationDto(BookRegistration bookRegistration) {
        return BookRegistrationDto.builder()
                .bookRegistrationId(bookRegistration.getBookRegistrationId())
                .guest(bookRegistration.getGuest())
                .book(bookRegistration.getBook())
                .registeredAt(bookRegistration.getRegisteredAt())
                .unregisteredAt(bookRegistration.getUnregisteredAt())
                .state(bookRegistration.getState())
                .build();
    }

    public static List<BookRegistrationDto> toBookRegistrationDtoList(List<BookRegistration> bookRegistrations) {
        return bookRegistrations.stream().map(BookRegistrationDto::toBookRegistrationDto).collect(Collectors.toList());
    }
}
