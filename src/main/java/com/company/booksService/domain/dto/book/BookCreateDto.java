package com.company.booksService.domain.dto.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a BookCreateDto class for Books Tracking system.
 */
@Value
@Builder
@Jacksonized
@AllArgsConstructor
public class BookCreateDto {

    String bookName;
    String bookDescription;
    Integer bookQuantity;

}
