package com.company.booksService.domain.dto.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a BookDto class for Books Tracking system.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookDto {

    private Long bookId;
    private String bookName;
    private String bookDescription;
    private Integer bookQuantity;
}
