package com.company.booksService.domain.dto.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a BookUpdateDto class for Book Tracking system.
 */
@Value
@Builder
@Jacksonized
@AllArgsConstructor
public class BookUpdateDto {

    String bookName;
    String bookDescription;
    Integer bookQuantity;
}
