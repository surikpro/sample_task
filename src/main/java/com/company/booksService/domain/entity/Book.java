package com.company.booksService.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Book Entity Class
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private Long bookId;
    @Column(name = "book_name")
    private String bookName;
    @Column(name = "book_description")
    private String bookDescription;
    @Column(name = "book_quantity")
    private Integer bookQuantity;

    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    List<BookRegistration> registrations;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "cover_id")
    @JsonIgnore
    private CoverInfo coverInfo;

//    @ManyToMany(mappedBy = "books")
//    @JsonIgnore
//    private List<Guest> guests = new ArrayList<>();
}




