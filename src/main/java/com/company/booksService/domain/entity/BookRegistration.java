package com.company.booksService.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Book Registration Class
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class BookRegistration {
    public enum State {
        TAKEN, RETURNED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_registration_id")
    Long bookRegistrationId;

    @ManyToOne
    @JoinColumn(name = "guest_id")
    Guest guest;

    @ManyToOne
    @JoinColumn(name = "book_id")
    Book book;

    private LocalDateTime registeredAt;

    private LocalDateTime unregisteredAt;

    @Enumerated(value = EnumType.STRING)
    private State state;

}
