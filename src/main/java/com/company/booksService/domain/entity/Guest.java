package com.company.booksService.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

import java.util.List;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Guest Entity Class
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Guest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "guest_id")
    private Long guestId;

    @Column(name = "guest_name")
    private String guestName;

    @OneToMany(mappedBy = "guest", fetch = FetchType.LAZY)
    @JsonIgnore
    List<BookRegistration> registrations;

//    @ManyToMany
//    @JoinTable(name = "orders",
//            joinColumns = @JoinColumn(name = "guest_id"),
//            inverseJoinColumns = @JoinColumn(name = "book_id"))
//    private List<Book> books = new ArrayList<>();

//    public void addBook(Book book) {
//        this.books.add(book);
//        book.getGuests().add(this);
//    }
//
//    public void removeBook(Book book) {
//        this.books.remove(book);
//        book.getGuests().remove(this);
//    }
}
