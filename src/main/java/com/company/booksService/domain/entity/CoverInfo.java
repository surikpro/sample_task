package com.company.booksService.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * This is CoverInfo Entity model for Books
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
public class CoverInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String storageFileName;

    private String originalFileName;

    private String mimeType;

    private Long size;

    @OneToOne(mappedBy = "coverInfo", fetch = FetchType.LAZY)
    private Book book;

    public Book getBook() {
        return this.book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
