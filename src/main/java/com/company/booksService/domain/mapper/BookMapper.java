package com.company.booksService.domain.mapper;

import com.company.booksService.domain.dto.book.BookCreateDto;
import com.company.booksService.domain.dto.book.BookDto;
import com.company.booksService.domain.dto.book.BookUpdateDto;
import com.company.booksService.domain.entity.Book;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Book Mapper Class
 */
@Mapper(componentModel = "spring")
public interface BookMapper {

    BookDto toBookDto(Book book);

    Book fromCreateDto(BookCreateDto bookCreateDto);

    Book fromUpdateDto(BookUpdateDto bookUpdateDto);

    @BeanMapping(nullValuePropertyMappingStrategy = IGNORE)
    Book merge(@MappingTarget Book target, Book source);

}
