package com.company.booksService.domain.mapper;

import com.company.booksService.domain.dto.guest.GuestCreateDto;
import com.company.booksService.domain.dto.guest.GuestDto;
import com.company.booksService.domain.entity.Guest;
import org.mapstruct.Mapper;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Guest Mapper Class
 */
@Mapper(componentModel = "spring")
public interface GuestMapper {
    Guest fromCreateDto(GuestCreateDto guestCreateDto);

    GuestDto toGuestDto(Guest guest);

}
