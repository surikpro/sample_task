package com.company.booksService.repositories;

import com.company.booksService.domain.entity.CoverInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**

 */
public interface CoversRepository extends JpaRepository<CoverInfo, Long> {
    CoverInfo getById(Long coverId);
    Optional<CoverInfo> findCoverInfoByBook_BookId(Long bookId);

}
