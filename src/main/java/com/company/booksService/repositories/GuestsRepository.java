package com.company.booksService.repositories;

import com.company.booksService.domain.entity.Guest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Tasks Repository class for CRUD operations.
 */
public interface GuestsRepository extends JpaRepository<Guest, Long> {
}
