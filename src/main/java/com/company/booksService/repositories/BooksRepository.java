package com.company.booksService.repositories;

import com.company.booksService.domain.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Books Repository class for CRUD operations.
 */
public interface BooksRepository extends JpaRepository<Book, Long> {
}
