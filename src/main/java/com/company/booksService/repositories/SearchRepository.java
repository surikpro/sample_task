package com.company.booksService.repositories;


import com.company.booksService.domain.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Search Repository class for CRUD operations.
 */
public interface SearchRepository extends JpaRepository<Book, Long> {
    List<Book> findByBookNameLike(String text);

    @Query(value = "SELECT * FROM book WHERE to_tsvector(book_name || ' ' || book_description) @@ to_tsquery(:text)",
            nativeQuery = true)
    List<Book> findBooks(@Param("text") String text);
}
