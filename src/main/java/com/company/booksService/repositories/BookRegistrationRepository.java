package com.company.booksService.repositories;

import com.company.booksService.domain.entity.BookRegistration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a BookRegistration Repository class for CRUD operations.
 */
public interface BookRegistrationRepository extends JpaRepository<BookRegistration, Long> {
    List<BookRegistration> findBookRegistrationsByGuest_GuestId(Long guestId);
//    Boolean existsBookRegistrationByGuest_GuestId(Long guestId);
    List<BookRegistration> getAllBooksByGuest_GuestId(Long guestId);
    List<BookRegistration> findAllByRegisteredAtBetween(LocalDateTime from, LocalDateTime to);
}
