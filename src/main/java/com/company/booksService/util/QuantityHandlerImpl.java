package com.company.booksService.util;

import com.company.booksService.domain.entity.Book;
import com.company.booksService.repositories.BooksRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Quantity Handler Class for handling its Quantity.
 */
@Service
@RequiredArgsConstructor
public class QuantityHandlerImpl implements QuantityHandler {

    private final BooksRepository booksRepository;

    @Override
    public boolean checkBookQuantity(Long bookId) {
        boolean isAvailable;
        Book book = booksRepository.getById(bookId);
        if (book.getBookQuantity() > 0) {
            isAvailable = true;
        } else {
            isAvailable = false;
        }
        return isAvailable;
    }

    @Override
    public void incrementBookQuantity(Long bookId) {
        Book book = booksRepository.getById(bookId);
        int quantity = book.getBookQuantity();
        book.setBookQuantity(++quantity);
        booksRepository.save(book);
    }

    @Override
    public void decrementBookQuantity(Long bookId) {
        Book book = booksRepository.getById(bookId);
        int quantity = book.getBookQuantity();
        book.setBookQuantity(--quantity);
        booksRepository.save(book);
    }
}
