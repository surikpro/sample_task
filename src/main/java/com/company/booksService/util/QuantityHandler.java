package com.company.booksService.util;

/**
 * by Aydar Zakirov
 * since 17.02.2022
 * This is a Quantity Handler Interface for handling Book Quantity.
 */
public interface QuantityHandler {

    boolean checkBookQuantity(Long bookId);

    void incrementBookQuantity(Long bookId);

    void decrementBookQuantity(Long bookId);
}
