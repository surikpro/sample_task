package com.company.booksService;

import com.company.booksService.domain.dto.book.BookCreateDto;
import com.company.booksService.domain.dto.book.BookUpdateDto;
import com.company.booksService.domain.entity.Book;
import com.company.booksService.domain.entity.Guest;
import com.company.booksService.domain.mapper.BookMapper;
import com.company.booksService.exceptions.BookNotFoundException;
import com.company.booksService.repositories.BooksRepository;
import com.company.booksService.repositories.GuestsRepository;
import com.company.booksService.services.BooksServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BooksServiceUnitTest {

    @InjectMocks
    private BooksServiceImpl booksServiceImpl;

    @Mock
    private GuestsRepository guestsRepository;
    @Mock
    private BooksRepository booksRepository;
    @Mock
    private BookMapper bookMapper;

    Book book = new Book();
    Guest guest = new Guest();

    @BeforeEach
    public void initData() {
        book.setBookId(2L);
        book.setBookName("Agatha Christie");
        book.setBookDescription("Interesting");
        book.setBookQuantity(10);
        guest.setGuestId(1L);
        guest.setGuestName("Max");
    }

    @Test
    public void testBookNotFoundById() {
        Mockito.when(booksRepository.getById(book.getBookId())).thenThrow(new BookNotFoundException(book.getBookId()));
        try {
            booksServiceImpl.get(book.getBookId());
            fail();
        } catch (BookNotFoundException e) {
            Assertions.assertEquals("Book with id: " + book.getBookId() + " is not found", e.getMessage());
        }
    }

    @Test
    public void testGetBookById() {
        Mockito.when(booksRepository.getById(book.getBookId())).thenReturn(book);
        Assertions.assertEquals(booksServiceImpl.get(book.getBookId()), book);
    }

    @Test
    public void testCreateBook() {
        booksServiceImpl.save(book);
        verify(booksRepository, times(1)).save(book);
    }
}
