#
#FROM maven:3-openjdk-17 AS build
#
#COPY src /usr/application/src/
#COPY pom.xml /usr/application
#USER root
#RUN --mount=type=cache,target=/root/.m2 mvn -DskipTests=true -f /usr/application/pom.xml clean package
#
#FROM openjdk:17-alpine3.13
#
#COPY --from=build /usr/application/target/app.jar /usr/local/lib/app.jar
#
#EXPOSE 8080:80
#
#ENTRYPOINT ["java", "-jar", "/usr/local/lib/app.jar"]

FROM maven:3-openjdk-17 AS build
COPY src /home/application/src
COPY pom.xml /home/application
USER root
RUN --mount=type=cache,target=root/.m2 mvn -f /home/application/pom.xml clean package

FROM openjdk:17-ea-17-jdk-slim
COPY --from=build /home/application/target/app.jar /usr/local/lib/app.jar
EXPOSE 80:80
ENTRYPOINT ["java", "-jar", "/usr/local/lib/app.jar"]
